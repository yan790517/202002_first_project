# Kaggler競賽 - Predict Future Sales

Kaggle競賽案例學習 - [Feature engineering, xgboost](https://www.kaggle.com/dlarionov/feature-engineering-xgboost#Part-1,-perfect-features)

Part 1, perfect features¶

``` python
# (加上自己的註解）匯入需要的套件
import numpy as np
import pandas as pd
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 100)

from itertools import product
from sklearn.preprocessing import LabelEncoder

import seaborn as sns
import matplotlib.pyplot as plt
%matplotlib inline

from xgboost import XGBRegressor
from xgboost import plot_importance

def plot_features(booster, figsize):    
    fig, ax = plt.subplots(1,1,figsize=figsize)
    return plot_importance(booster=booster, ax=ax)

import time
import sys
import gc
import pickle
sys.version_info


items = pd.read_csv('../input/items.csv')
shops = pd.read_csv('../input/shops.csv')
cats = pd.read_csv('../input/item_categories.csv')
train = pd.read_csv('../input/sales_train.csv')
# set index to ID to avoid droping it later
test  = pd.read_csv('../input/test.csv').set_index('ID')


# Outliers
# (加上自己的註解）確認item_cnt_day、item_price的box圖，確認資料分佈以及是否有outlier值
plt.figure(figsize=(10,4))
plt.xlim(-100, 3000)
sns.boxplot(x=train.item_cnt_day)

plt.figure(figsize=(10,4))
plt.xlim(train.item_price.min(), train.item_price.max()*1.1)
sns.boxplot(x=train.item_price)


# 底下的Code請大家各自加上說明文字，作為第一週的作業

train = train[train.item_price<100000] # 訓練資料 item_price取小於100,000
train = train[train.item_cnt_day<1001] # 訓練資料 item_cnt_day取小於1,001

# 篩選出"商店編號32"&"產品編號2973"&"March2013"&"產品價格大於0"的測試資料中，產品價格的中位數
median = train[(train.shop_id==32)&(train.item_id==2973)&(train.date_block_num==4)&(train.item_price>0)].item_price.median()
train.loc[train.item_price<0, 'item_price'] = median #產品價格小於0的測試資料，item_price內的值改成median


train.loc[train.shop_id == 0, 'shop_id'] = 57 # 訓練資料 shop_id為0時，shop_id值改成57
test.loc[test.shop_id == 0, 'shop_id'] = 57 # 測試資料 shop_id為0時，shop_id值改成57
# Якутск ТЦ "Центральный"
train.loc[train.shop_id == 1, 'shop_id'] = 58 # 訓練資料 shop_id為1時，shop_id值改成58
test.loc[test.shop_id == 1, 'shop_id'] = 58 # 測試資料 shop_id為1時，shop_id值改成58
# Жуковский ул. Чкалова 39м²
train.loc[train.shop_id == 10, 'shop_id'] = 11 # 訓練資料 shop_id為10時，shop_id值改成11
test.loc[test.shop_id == 10, 'shop_id'] = 11 # 測試資料 shop_id為10時，shop_id值改成11


# Shops/Cats/Items preprocessing
shops.loc[shops.shop_name == 'Сергиев Посад ТЦ "7Я"', 'shop_name'] = 'СергиевПосад ТЦ "7Я"' # shops資料中，shop_name為Сергиев Посад ТЦ "7Я"改成'СергиевПосад ТЦ "7Я"'
shops['city'] = shops['shop_name'].str.split(' ').map(lambda x: x[0]) # shops資料中，shop_name以' '切分的第一段取city
shops.loc[shops.city == '!Якутск', 'city'] = 'Якутск' # shops資料中，city為!Якутск改成Якутск
shops['city_code'] = LabelEncoder().fit_transform(shops['city']) # shops資料的city做LabelEncoder，並新增為city_code
shops = shops[['shop_id','city_code']] # shops資料只取shop_id、city_code兩欄位

cats['split'] = cats['item_category_name'].str.split('-') # cats資料item_category_name以'-'切分，並新增成split欄位
cats['type'] = cats['split'].map(lambda x: x[0].strip()) # split移除空格並新增成type欄位
cats['type_code'] = LabelEncoder().fit_transform(cats['type']) # type做LabelEncoder，並新增為type_code
# if subtype is nan then type
cats['subtype'] = cats['split'].map(lambda x: x[1].strip() if len(x) > 1 else x[0].strip()) #如果split長度大於1，則取第二順位並移除空格，反之則取第一順位並移除空格，並新增成subtype
cats['subtype_code'] = LabelEncoder().fit_transform(cats['subtype']) # subtype做LabelEncoder，並新增為subtype_code
cats = cats[['item_category_id','type_code', 'subtype_code']] # cats資料只取item_category_id、type_code、subtype_code三欄位

items.drop(['item_name'], axis=1, inplace=True) # 移除items資料中的item_name欄位

# Monthly sales
len(list(set(test.item_id) - set(test.item_id).intersection(set(train.item_id)))), len(list(set(test.item_id))), len(test) # test.item_id集合交集長度、test.item_id集合長度、test長度

ts = time.time()
matrix = []
cols = ['date_block_num','shop_id','item_id']

# 迴圈到34，date_block_num亦到34，每個對應的product/shop_id/item_id，取16位整數，轉numpy並append成matrix
for i in range(34):
    sales = train[train.date_block_num==i] 
    matrix.append(np.array(list(product([i], sales.shop_id.unique(), sales.item_id.unique())), dtype='int16'))
    
matrix = pd.DataFrame(np.vstack(matrix), columns=cols) # matrix轉成直行DataFrame
matrix['date_block_num'] = matrix['date_block_num'].astype(np.int8) # date_block_num換成8位整數
matrix['shop_id'] = matrix['shop_id'].astype(np.int8) # shop_id換成8位整數
matrix['item_id'] = matrix['item_id'].astype(np.int16) # item_id換成16位整數
matrix.sort_values(cols,inplace=True) # matrix以cols排序
time.time() - ts

train['revenue'] = train['item_price'] *  train['item_cnt_day'] # revenue=item_price*item_cnt_day

ts = time.time()
group = train.groupby(['date_block_num','shop_id','item_id']).agg({'item_cnt_day': ['sum']}) # 以date_block_num/shop_id/item_id做groupby，加總item_cnt_day
group.columns = ['item_cnt_month'] # group欄位名改item_cnt_month
group.reset_index(inplace=True) # group reset index

matrix = pd.merge(matrix, group, on=cols, how='left') # matrix left join group，令成matrix

# item_cnt_month，na轉0、值取0~20，轉16位浮點數
matrix['item_cnt_month'] = (matrix['item_cnt_month']
                                .fillna(0)
                                .clip(0,20) # NB clip target here
                                .astype(np.float16))
time.time() - ts


```

